require 'facebook/messenger'
include Facebook::Messenger
# NOTE: ENV variables should be set directly in terminal for testing on localhost

# Subcribe bot to your page
Facebook::Messenger::Subscriptions.subscribe(access_token: ENV["ACCESS_TOKEN"])

Bot.on :message do |message|
  puts "Received '#{message.inspect}' from #{message.sender}"

  if message.text.include? ".png" or message.text.include? ".jpg"
    sent_text=message.text
    puts "received: \"#{sent_text[1]}\""
    begin
      message.reply(
        attachment: {
          type: 'image',
          payload: {
            url: sent_text
          }
        }
      )
    rescue
      message.reply(text: "looks rough")
    end
    puts "sent"
  else
    message.reply(text: 'Harh?')
  end
end
