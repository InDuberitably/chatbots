var system = require('system');
var args = system.args;
var steps=[];
var testindex = 0;
var loadInProgress = false;//This is set to true when a page is still loading

if (args.length != 3) {
throw new Error("Need a username and password as arguments: phantomjs PilotCalendar.js username password!");  
} else {
  args.forEach(function(arg, i) {
    console.log(i + ': ' + arg);
  });
} 
/*********SETTINGS*********************/
var webPage = require('webpage');
var page = webPage.create();
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36';
page.settings.javascriptEnabled = true;
page.settings.loadImages = false;//Script is much faster with this field set to false
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;
/*********SETTINGS END*****************/
 
console.log('All settings loaded, start with execution');
page.onConsoleMessage = function(msg) {
    console.log(msg);
};
/**********DEFINE STEPS THAT PHANTOM SHOULD DO***********************/
steps = [ 
    //Step 1 - Open Pilot home page
    function a(){
        console.log('Step 1 - Open Pilot home page');
        page.open("https://pilot.wright.edu/login.asp", function(status){});
    },
    function b(){
        var system = require('system');
        var args = system.args;
        console.log('Step 2 - Populate and submit the login form');
        page.evaluate(function(args){

            document.getElementById("Username").value=args[1];
            document.getElementById("Password").value=args[2];
            document.forms[0].submit();
        
        }, args);
    },
    //Step 3 - Wait Pilot to login user. After user is successfully logged in, user is redirected to home page. Content of the home page is saved to PilotLoggedIn.html. You can find this file where phantomjs.exe file is. You can open this file using Chrome to ensure that you are logged in.
    function c(){
        console.log("Step 3 - Wait Pilot to login user. After user is successfully logged the home page is saved to PilotLoggedIn.html. You can find this file where phantomjs.exe file is. You can open this file using Chrome to ensure that you are logged in.");
         var fs = require('fs');
         var result = page.evaluate(function() {
            return document.querySelectorAll("html")[0].outerHTML;});
        fs.write('PilotLoggedIn.html',result,'w');
    },
];

/**********END STEPS THAT PHANTOM SHOULD DO***********************/
 
//Execute steps one by one
interval = setInterval(executeRequestsStepByStep,50);
 
function executeRequestsStepByStep(){
    if (loadInProgress == false && typeof steps[testindex] == "function") {
        //console.log("step " + (testindex + 1));
        steps[testindex]();
        testindex++;
    }
    if (typeof steps[testindex] != "function") {
        console.log("test complete!");
        phantom.exit();
    }
}
 
/**
 * These listeners are very important in order to phantom work properly. Using these listeners, we control loadInProgress marker which controls, weather a page is fully loaded.
 * Without this, we will get content of the page, even a page is not fully loaded.
 */
page.onLoadStarted = function() {
    loadInProgress = true;
    console.log('Loading started');
};
page.onLoadFinished = function() {
    loadInProgress = false;
    console.log('Loading finished');
};
page.onConsoleMessage = function(msg) {
    console.log(msg);
};

//d2l-datalist vui-list
